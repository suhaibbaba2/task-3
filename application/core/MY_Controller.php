<?php

require APPPATH . '/libraries/REST_Controller.php';


class MY_Controller extends REST_Controller
{

    public function __construct()
    {

        parent::__construct();
    }

    public function isLogin()
    {
        if ($this->session->userdata('islogin') == 'true')
            return true;
        else
            return false;
    }

    public function Get_userName()
    {
        return $this->session->userdata('user')['name'];
    }

    public function Get_userId()
    {
        return $this->session->userdata('user')['id'];
    }

    public function Get_userEmail(){
        return $this->session->userdata('user')['email'];
    }

    public function Get_Type(){
        return $this->session->userdata('user')['type'];
    }

    public function Set_login($data){

        $this->session->set_userdata('islogin', 'true');

        $user = array(
            'name' => $data['name'],
            'id' => $data['id'],
            'email' => $data['email'],
            'type'=>$data['type']);
        $this->session->set_userdata('user', $user);


    }

    public function SetHeader($title=""){
        $header['title']=$title;
        $header['islogin']=$this->isLogin();
        $header['userName']=$this->Get_userName();
        $header['isTeacher']=$this->Get_Type()=="teacher";

        return $header;
    }

}

