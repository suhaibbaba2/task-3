<?php
Class Users extends CI_Model {

    Public function __construct() {
        parent::__construct();
    }

    public function insets($data){

        $this->db->insert('users',$data);
        return $this->db->insert_id();
    }

    public function checkLogin($data){
        $this->db->select('firstname, type,id');
        $query = $this->db->get_where('users', array('email' => $data['email'],'password'=>$data['password']));

        $query_result=$query->result_array();

        if(empty($query_result))
            return false;

        $result=array('name'=>$query_result[0]['firstname'],'type'=>$query_result[0]['type'],'id'=>$query_result[0]['id']);
        return $result;

    }

}
?>