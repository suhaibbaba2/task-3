<?php


class Subjects extends CI_Model
{
    Public function __construct() {
        parent::__construct();
    }

    public function GetAll($year){

        $this->db->select('id,subject');
        $query_result = $this->db->get_where('subjects',array('year'=>$year));
        if(empty($query_result))
            return false;

        $result=[];
        foreach ($query_result->result() as $row)
        {
            array_push($result,array("id"=>$row->id,"subject"=>$row->subject));
        }


        return $result;
    }

    public function inserts($data){

       return $this->db->insert('subjects',$data);

    }
}