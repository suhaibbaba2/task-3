<?php

/**
 * Created by PhpStorm.
 * User: user
 * Date: 6/20/2017
 * Time: 12:11 AM
 */
class Articles extends CI_Model
{


    public function GetAll($input){

        $query_result=$this->db->get_where('articles',$input);

        $result=[];
        foreach ($query_result->result() as $row)
        {
            array_push($result,array("id"=>$row->id,"title"=>$row->title,'author'=>$row->author));
        }

        return $result;

    }

    public function GetArticle($input){
        $query_result=$this->db->get_where('articles',$input);

        $result=$query_result->result_array();

        return $result;

    }

    public function GetUserID($input){
        $this->db->select('id_user');
        $query_result=$this->db->get_where('articles',$input);

        $result=$query_result->result_array();

        return $result;

    }

    public function GetFiles($input){
        $query_result=$this->db->get_where('files',$input);

        $result=$query_result->result_array();

        return $result;

    }

    public function DeleteArticle($id){
        $this->db->delete('articles',array('id'=>$id));
    }

    public function Inserts($input){
        $this->db->insert('articles',$input);
        return $this->db->insert_id();
    }

    public function InsertsFile($input){
        return $this->db->insert('files',$input);
    }

    public function Updates($input,$id){
        $this->db->where('id', $id);
        return $this->db->update('articles', $input);
    }

    public function GetComment($input_file)
    {
        $this->db->order_by('id', 'DESC');
        $this->db->select('id,comment,StudentName,id_user');
        $query_result=$this->db->get_where('comments',$input_file);

        $result=$query_result->result_array();

        return $result;

    }

    public function GetCommentUserId($input_file)
    {
        $this->db->select('id_user');
        $query_result=$this->db->get_where('comments',$input_file);

        $result=$query_result->result_array();

        return $result;

    }

    public function InsertComment($input){
        return $this->db->insert('comments',$input);
    }
    public function UpdateComment($input){
        $this->db->where('id', $input['id']);
        return $this->db->update('comments', $input);
    }

    public function DeleteComment($input){
        return $this->db->delete('comments',$input);

    }

    public function RemoveFile($input){
        $this->db->where_in('id', $input);

        return $this->db->delete('files');
    }
}