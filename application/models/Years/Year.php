<?php

/**
 * Created by PhpStorm.
 * User: user
 * Date: 6/19/2017
 * Time: 3:36 PM
 */
class Year extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
    }

    public function GetAll(){
        $this->db->order_by('year', 'DESC');
        $this->db->select('year');
        $this->db->distinct();
        $query_result = $this->db->get('subjects');
        if(empty($query_result))
            return false;

        $result=[];
        foreach ($query_result->result() as $row)
        {
            array_push($result,$row->year);
        }

        return $result;

    }

}