<?php

class Searchs extends CI_Model{
    public function __construct()
    {
        parent::__construct();
    }

    public function GetSearch_Subjects($input){
        $this->db->like($input);
        $this->db->select('subject,id,year');
        $result=$this->db->get('subjects');
        return ($result->result_array());

    }

    function GetSearch_Articles($input){
        $this->db->like($input);
        $this->db->select('id,title');
        $result=$this->db->get('articles');
        return ($result->result_array());

    }
}