<div class="container index-subject">

    <div id="Control-subjects" class=" page-header row text-right">
        <span style="color: #0000FF"><?=$year?></span>
        <a href="<?= base_url(); ?>years">
            <img src="<?= base_url(); ?>/public/images/calendar-clock.png">
        </a>
        <?php
        if ($isLogin && $isTeacher) {
            ?>
            <a href="<?= base_url(); ?>subject/create" class="btn btn-success"><i class="glyphicon glyphicon-plus"></i>
                New Subject</a>
            <?php
        }
        ?>
    </div>
    <div class="row">
        <?php
        if (count($data) > 0) {
            foreach ($data as $item):
                ?>
                <div class="col-lg-2 col-sm-4 col-xs-6" style="margin-bottom: 20px;">
                    <a class="item" href="<?= base_url() . 'article/show/' . urlencode($item['id'])
                    . '/' . urlencode($item['subject']) . '/' . urlencode($year) ?>">
                        <img class="center-block" src="<?= base_url(); ?>/public/images/folder-blue.png">
                        <h3 class="text-center"><?= ucfirst($item['subject']); ?></h3>
                    </a>
                </div>
                <?php
            endforeach;
        } else {
            echo "<h1 class='text-center' style='font-weight: 600;color: #56120c;font-size: 50px'>No Data</h1>";
        }
        ?>

        <div>
        </div>

        <script>
            $('.item').hover(function () {

                $(this).children('img').attr('src', '<?= base_url();?>/public/images/folder-green.png');

            }, function () {

                $(this).children('img').attr('src', '<?= base_url();?>/public/images/folder-blue.png');

            });
        </script>


