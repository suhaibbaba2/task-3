<link rel="stylesheet" href="<?= base_url();?>/public/css/bootstrap-datepicker.min.css">
<div class="container create-subject">
    <div class="row img-class center-block">
        <img src="<?= base_url();?>/public/images/course-icon.png" alt="Course" class="text-center center-block">
    </div>
    <form method="post" action="<?=base_url();?>subject/insert">
        <div class="row center-block text-center">
            <input type="text" class="form-control" placeholder="Name" name="name" required >
        </div>
        <div class="row center-block text-center">
            <input type="text" class="form-control" id="year" placeholder="Year" data-date-format="yyyy" name="year" required readonly>
        </div>
        <div class="row text-center center-block">
            <input type="submit" class="btn btn-success" value="New Subject">
        </div>
    </form>

</div>

<script src="<?= base_url()?>/public/js/bootstrap-datepicker.min.js"></script>
<script>
    $('#year').datepicker({
        format: "yyyy",
        autoclose: true,
        minViewMode: "years"
    })    .on('changeDate', function(selected){
        startDate =  $("#from").val();
        $('#to').datepicker('setStartDate', startDate);
    });
    ;
</script>
