<?php $this->load->view('header/header', $header); ?>


<div class="container index-article">
    <?php
        if($isTeacher) {
            ?>
            <div id="NewArticleButton" class="page-header text-right">
                <a class="btn btn-success" href="<?= base_url() ?>article/create"><i
                            class="glyphicon glyphicon-plus"></i> New Article</a>
            </div>
            <?php
        }
    ?>
    <div class="row">
        <?php
        if(count($data)>0) {
            foreach ($data as $item):
                ?>
                <div class="col-lg-2 col-sm-4 col-xs-12" style="margin-bottom: 20px;">
                    <a class="item" href="<?= base_url(); ?>article/content/<?= $item['id']; ?>/<?= urlencode($item['title']); ?>">
                        <img class="center-block" src="<?= base_url(); ?>/public/images/article.png">
                        <h3 class="text-center"><?= $item['title']; ?></h3>
                        <h5 class="text-center"><?= $item['author']; ?></h5>
                    </a>
                </div>
                <?php
            endforeach;
        }
        ?>
    </div>
</div>

<script>
    $('.item').hover(function () {

        $(this).children('img').attr('src','<?= base_url();?>/public/images/article-red.png');

    },function () {

        $(this).children('img').attr('src','<?= base_url();?>/public/images/article.png');

    });
</script>