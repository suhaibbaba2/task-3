<div class="container create-article">
    <div class="page-header">
        <h1>New Article</h1>
    </div>

    <div class="row">
        <form method="post" action="<?=base_url();?>article/insert" enctype="multipart/form-data">
            <div class="col-lg-12">
                <input type="text" class="form-control" name="title" placeholder="Title" style="width: 40%" value="" required>
            </div>
            <div class="col-lg-12">
                <input type="text" class="form-control" name="author" placeholder="Author" value="" style="width: 40%" required>
            </div>

            <div class="col-lg-12">
                <input type="file" name="fileToUpload[]" id="fileToUpload" multiple>
            </div>

            <div class="col-lg-12">
                <textarea id="content" name="content">
                </textarea>
            </div>
            <div class="col-lg-12 text-center CreateArticleButton">
                <button onclick="" class="btn btn-success">Create Article</button>
            </div>
        </form>
    </div>
</div>
<script src="<?=base_url();?>public/js/tinymce.min.js"></script>
<script src="<?=base_url();?>public/js/jquery.tinymce.min.js"></script>
<script>
    tinymce.init({
        selector: 'textarea',
        height: 250,
        menubar: false,
        toolbar: 'undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image',
    });

</script>
