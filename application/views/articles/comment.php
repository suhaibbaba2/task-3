<div class="row">
    <div class="comment-box">
        <?php
        if($UserIDLogin==$UserID) {
            ?>

            <button href="#"  data-value="<?=$id;?>" id="deleteComment"
                    onclick="DeleteComment()" class="pull-right btn btn-danger" style="margin: 0px 10px">
                <i class="glyphicon glyphicon-remove"></i>
            </button>
            <button id="editComment" href="#"   data-value="<?=$id;?>" class="pull-right btn btn-warning "
                    onclick="EditComemnt()" style="margin: 0px 10px">
                <i class="glyphicon glyphicon-edit"></i>
            </button>
            <?php
        }
        ?>
            <h5><?= $StudentName; ?></h5>
            <p id="comment-display"><?= $comment; ?></p>
            <textarea id="comment-edit"  class="form-control center-block" style="display: none"><?= $comment; ?></textarea>

            <button id="updateComment"
                    class="btn btn-primary center-block" onclick="UpdateComment()" style="margin-top: 5px;display: none;">Update</button>
    </div>
</div>

