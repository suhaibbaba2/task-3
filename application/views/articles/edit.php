<div class="container create-article">
    <div class="page-header">
        <h1>New Article</h1>
    </div>

    <div class="row">
        <form method="post" action="<?=base_url();?>article/update"  enctype="multipart/form-data">
            <div class="col-lg-12">
                <input type="text" class="form-control" name="title" placeholder="Title" style="width: 40%"
                       value="<?=$data[0]['title']?>" required>
            </div>
            <div class="col-lg-12">
                <input type="text" class="form-control" name="author" placeholder="Author"
                       value="<?=$data[0]['author']?>" style="width: 40%" required>
            </div>

            <div class="col-lg-12 remove-item" style="margin: 10px 0">
                <?php
                if(count($result_files)>0) {
                    foreach ($result_files as $item):
                        ?>
                        <h4><?= $item['name']; ?>&nbsp;<span onclick="RemvoeFile(this)" data-value="<?=$item['id'];?>" style="cursor: pointer"><i class="glyphicon glyphicon-remove" style="color: #d9534f;"></i></span></h4>
                        <?php
                    endforeach;
                }
                ?>
            </div>
            <div class="col-lg-12" style="margin: 20px 0">
                <input type="file" name="fileToUpload[]" id="fileToUpload" multiple>
            </div>

            <div class="col-lg-12">
                <textarea id="content" name="content">
                    <?=$data[0]['body']?>
                </textarea>
            </div>
            <div class="col-lg-12 text-center CreateArticleButton">
                <button class="btn btn-warning">Update Article</button>
            </div>
        </form>

    </div>
</div>
<script src="<?=base_url();?>public/js/tinymce.min.js"></script>
<script src="<?=base_url();?>public/js/jquery.tinymce.min.js"></script>
<script>
    tinymce.init({
        selector: 'textarea',
        height: 250,
        menubar: false,
        toolbar: 'undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image',
    });

    file_remove=[];
    function RemvoeFile(e) {
        file_remove.push($(e).data('value'))
        $(e).parent().remove();
    }


    function edit_file_send() {
        id_data=file_remove;
        if(id_data.length<=0){
            return;
        }
        console.log(id_data)
        var data={
            'id'       : id_data
        };
        $.ajax({
            url:        "<?= base_url();?>article/removeFiles",
            type:       'POST',
            data:       data,
            cache:      false,
            success: function(result){
                console.log(result)
            },
            error:function (result){
                console.log(result);
            }
        });

    }

    $("form").submit(function(e){
        edit_file_send();
        return true;

    });

</script>
