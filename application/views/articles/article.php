<div class="container article-content">
    <?php
    if (count($data) <= 0)
        redirect(base_url());
    ?>
    <div class="page-header">
        <?php
        if ($isWriter) {
            ?>
            <div class="text-right">

                <a href="<?= base_url(); ?>article/edit/<?= $data[0]['id'] ?>" class="btn btn-warning text-right"
                   style="width: 128px;margin-right: 15px;"> <i class="glyphicon glyphicon-edit"></i> Edit Article</a>
                <a href="<?= base_url(); ?>article/delete/<?= $data[0]['id'] ?>" class="btn btn-danger text-right"> <i
                            class="glyphicon glyphicon-remove"></i> Delete Article</a>
            </div>
            <?php
        }
        ?>
        <h1><?= $data[0]['title']; ?></h1>
        <h4 style="text-indent: 20px"><strong style="text-transform: capitalize"><?= $data[0]['author'] ?></strong></h4>
    </div>

    <div id="body" class="row ">
            <?= $data[0]['body'] ?>
    </div>

    <div class="attachment">

        <div class="row">
            <div class="page-header">
                <h1 style="">Attachment
                    <i id="attachment-icon" class="glyphicon glyphicon-paperclip"></i>
                </h1>
            </div>
        </div>

        <div class="row files">
            <?php
            if (count($files) > 0) {
                foreach ($files as $item):
                    $img_name = 'file.png';

                    switch (strtolower($item['type'])) {
                        case 'pdf':
                            $img_name = 'pdf.png';
                            break;
                        case 'doc':
                            $img_name = 'doc.png';
                            break;
                        case 'docx':
                            $img_name = 'doc.png';
                            break;
                        case 'exe':
                            $img_name = 'exe.png';
                            break;
                        case 'js' :
                            $img_name = 'js.png';
                            break;
                        case 'php':
                            $img_name = 'php.png';
                            break;
                        case 'ppt':
                            $img_name = 'ppt.png';
                            break;
                        case 'rar':
                            $img_name = 'rar.png';
                            break;
                        case 'zip':
                            $img_name = 'zip.png';
                            break;
                        case 'png':
                            $img_name = 'png.png';
                            break;
                        case 'jpg':
                            $img_name = 'jpg.png';
                            break;
                        default:
                            $img_name = 'file.png';
                    }
                    ?>
                    <div class="col-lg-2 col-md-3 col-sm-4 col-xs-5">
                        <a href="<?= base_url(); ?>public/uploadFiles/<?= urlencode($item['name']) . '.' . urlencode($item['type']) ?>">
                            <img class="text-center" src="<?= base_url(); ?>public/images/file-images/<?= $img_name; ?>"
                                 title="<?= $item['name']; ?>" alt="<?= $item['name']; ?>">
                            <h5 class="text-center" style="width: 128px"><?= $item['name']; ?></h5>
                        </a>
                    </div>
                    <?php
                endforeach;
            } else {
                echo "<h3 class='text-center'>No Files</h3>";
            }
            ?>
        </div>

    </div>

    <div class="comment">

        <div class="row">
            <div class="page-header">
                <h1 style="">Comment
                    <i id="attachment-icon" class="glyphicon glyphicon-paperclip"></i>
                </h1>
            </div>

            <div class="row" style="margin-bottom: 40px;margin-left: 2%">
                <div class="col-lg-8">
                    <textarea id="comment" class="form-control" placeholder="Comment" required></textarea>
                </div>
                <div class="col-lg-4">
                    <button onclick="insert_comment()" class="btn btn-success" style="margin-top: 12px;border-radius: 20px">Send</button>
                </div>
            </div>
            <div class="updateComment">

            </div>
        </div>


    </div>
</div>
<script>

    $("#comment").on('keydown', function (e) {
        var code = e.keyCode || e.which;
        if(code == 13) { //Enter keycode
            insert_comment();
        }
    })

    function refresh_Comment(){
        $.ajax({
            url:        "<?= base_url();?>article/GetComment/<?=$data[0]['id'];?>",
            type:       'GET',
            cache:      false,
            success: function(html){
                $('.updateComment').html(html)
            }
        });

    }

    var isEditComment=true;//to Check if you edit Comment

    refresh_Comment();

    setInterval(function(){
        if(isEditComment) {
            refresh_Comment() // this will run after every 5 seconds
        }
    }, 5000);

    function EditComemnt() {
        isEditComment=!isEditComment;
        $("#comment-display").fadeToggle(200);
        $("#deleteComment").fadeToggle(200);
        $("#editComment").fadeToggle(200);
        $("#comment-edit").fadeToggle(200);
        $("#updateComment").fadeToggle(200);

    }

    function insert_comment() {
        var data={
            'comment'       : $("#comment").val() ,
            'id_article'    : <?=$data[0]['id'];?>,
            };
        $.ajax({
            url:        "<?= base_url();?>article/insertComment",
            type:       'POST',
            data:       data,
            cache:      false,
            success: function(){
                $("#comment").val('');
                refresh_Comment();            },
            error:function () {
                refresh_Comment();            }
        });

    }

    function UpdateComment() {

        var data = {
            'comment': $("#comment-edit").val(),
            'id': $("#editComment").data('value')
        };

        $.ajax({
            url: "<?= base_url();?>article/updateComment",
            type: 'POST',
            data: data,
            cache: false,
            success: function (data) {
                EditComemnt();
                refresh_Comment();

            }, error: function (data) {
            }
        });

    }

    function DeleteComment() {

        var data = {
            'id': $("#editComment").data('value')
        };

        $.ajax({
            url: "<?= base_url();?>article/deleteComment",
            type: 'POST',
            data: data,
            cache: false,
            success: function (data) {
                EditComemnt();
                refresh_Comment();

            }, error: function (data) {
            }
        });

    }

</script>