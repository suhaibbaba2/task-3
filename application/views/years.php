<div class="container years">
    <div class="row">
        <?php
        if(count($data)>0) {
            foreach ($data as $item):
                ?>
                <div class="col-lg-3 text-center">
                    <a href="<?= base_url(); ?>subjects/<?= $item; ?>"
                       class="btn btn-default text-center"><?= $item; ?></a>
                </div>
                <?php
            endforeach;
        }else{
            echo "<h1 class='text-center' style='font-weight: 600;color: #56120c;font-size: 50px'>No Data</h1>";
        }
        ?>
    </div>
</div>