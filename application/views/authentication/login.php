<div class="container authentication">
    <div class="box">
        <img src="<?=base_url();?>/public/images/login.png" style="width: 80px;height: 80px;">

        <form method="post" action="<?=base_url();?>checklogin">
            <input class="form-control text-center" type="email" name="email" placeholder="Email" required>
            <input class="form-control text-center" type="password" name="password" placeholder="Password" required>
            <input class="btn btn-success center-block" name="submit" type="submit" value="Login ">
        </form>

        <a href="<?= base_url();?>register" class="text-center center-block" style="margin-top: 40px">Register</a>

    </div>
</div>