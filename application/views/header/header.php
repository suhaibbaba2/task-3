</<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">

    <link rel="stylesheet" href="<?= base_url(); ?>public/css/bootstrap.min.css">
    <link rel="stylesheet" href="<?= base_url(); ?>public/css/style.css">
    <script src="<?= base_url(); ?>public/js/jquery_min.js"></script>
    <script src="<?= base_url(); ?>public/js/bootstrap.min.js"></script>
    <script src="<?= base_url(); ?>public/js/script.js"></script>
    <title><?= ucfirst($title); ?></title>
</head>
<body>

<!--
* Navbar for all pages
-->
<nav class="navbar navbar-inverse">
    <div class="container-fluid">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>

            <a class="navbar-brand" href="<?= base_url(); ?>"><span style="font-weight: 600">E</span> learning</a>
        </div>
        <div class="collapse navbar-collapse" id="myNavbar">

            <ul class="nav navbar-nav">
                <!--                -->
            </ul>

            <ul class="nav navbar-nav navbar-right">
                <?php
                if ($islogin) {
                    ?>
                    <li><a id="search-button" onclick="ShowSearchBox()" class="glyphicon glyphicon-search"
                           style="cursor: pointer;font-size: 20px"></a></li>

                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true"
                           aria-expanded="false"><i class="glyphicon glyphicon-user"></i>
                            &nbsp;&nbsp;<?= ucfirst($userName); ?> <span
                                    class="caret"></span></a>
                        <ul class="dropdown-menu">
                            <?php
                            if ($isTeacher) {
                                ?>
                                <li><a href="<?= base_url(); ?>student"><span
                                                class="glyphicon glyphicon-plus"></span> Add Student</a></li>

                                <li role="separator" class="divider"></li>
                                <?php
                            }
                            ?>
                            <li><a href="<?= base_url(); ?>logout"><span
                                            class="glyphicon glyphicon-log-out"></span> Logout</a></li>
                        </ul>
                    </li>
                    <?php
                } else {
                    ?>
                    <li><a href="<?= base_url(); ?>register"><span class="glyphicon glyphicon-user"></span> Sign Up</a>
                    </li>
                    <li><a href="<?= base_url(); ?>login"><span class="glyphicon glyphicon-log-in"></span> Login</a>
                    </li>
                    <?php
                }
                ?>

            </ul>

        </div>
    </div>
</nav>

<div class="container center-block" style="margin: 0 auto;">
    <div class="search-box row">
        <div class="dropdown col-lg-12">
            <div class="form-group has-feedback">
                <input id="search" type="search"
                       class="dropbtn form-control" onkeyup="Search(this.value , '<?= base_url(); ?>')"
                       placeholder="Search" style="margin-bottom: 40px;    padding-left: 35px;">
                <span class="glyphicon glyphicon-search form-control-feedback" style="left: 0;color: #ccc"></span>
            </div>
            <div id="livesearch" class="dropdown-content col-lg-11" style="left: 16px !important;">
            </div>

        </div>

    </div>
</div>


