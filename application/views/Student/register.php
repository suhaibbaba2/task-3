<style>

</style>

<div class="container authentication register">
    <div class="box">
        <img src="<?=base_url();?>/public/images/login.png">
        <p class="text-center" >For Student</p>
        <form method="post" action="<?=base_url();?>student/insert">
            <input class="form-control text-center" type="text" name="firstname" placeholder="First Name" required>
            <input class="form-control text-center" type="text" name="lastname" placeholder="Last Name" required>
            <input class="form-control text-center" type="email" name="email" placeholder="Email" required>
            <input class="form-control text-center" type="password" name="password" placeholder="Password" required>
            <input class="btn btn-primary center-block" name="submit" type="submit" value="Register ">
        </form>

        <a href="<?= base_url();?>login" class="text-center center-block">login</a>

    </div>
</div>