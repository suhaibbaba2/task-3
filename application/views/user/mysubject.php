<div class="container">
    <table class="table table-hover text-center">
        <thead>
        <tr>
            <th class="text-center">&nbsp;<input type="checkbox"  onchange="checkAll(this)"  ></th>
            <th class="text-center">My Subject</th>
            <th class="text-center">Year</th>
            <th class="text-center"></th>
        </tr>
        </thead>
        <tbody>
        <?php
        for($i=0;$i<10;$i++) {
            ?>
            <tr>
                <td><input type="checkbox"  name="id_input" ></td>
                <td>Doe</td>
                <td>2007</td>
                <td><a href="#" class="btn btn-danger"> <i class="glyphicon glyphicon-remove"></i> Delete Subject</a></td>
            </tr>
            <?php
        }
        ?>

        </tbody>
    </table>

    <script>
        function checkAll(e) {
            var checkboxes = document.getElementsByName('id_input   ');
            if (e.checked) {
                for (var i = 0; i < checkboxes.length; i++) {
                    if (checkboxes[i].type == 'checkbox') {
                        checkboxes[i].checked = true;
                    }
                }
            } else {
                for (var i = 0; i < checkboxes.length; i++) {
                    if (checkboxes[i].type == 'checkbox') {
                        checkboxes[i].checked = false;
                    }
                }
            }
        }
    </script>


</div>