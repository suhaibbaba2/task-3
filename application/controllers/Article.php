<?php

/**
 * Created by PhpStorm.
 * User: user
 * Date: 6/19/2017
 * Time: 11:53 PM
 */
class Article extends MY_Controller
{

    public function __construct()
    {
        parent::__construct();

        if (!$this->isLogin())
            redirect(base_url() . 'login');

        $this->load->model('articles/articles');

    }


    public function article_get()
    {
        base_url();
    }


    public function show_get($id_subject, $subject, $year)
    {

        $subject = urldecode($subject);
        $id_subject = urldecode($id_subject);
        $year = urldecode($year);

        $this->session->set_userdata(
            'subject', array(
            'id_subject' => $id_subject,
            'year' => $year,
            'subject' => $subject,
        ));
        $input = array('id_subject' => $id_subject, 'year' => $year);

        $result = $this->articles->GetAll($input);
        $data['data'] = $result;
        $data['isTeacher'] = $this->Get_Type() == "teacher";

        $header = $this->SetHeader($subject);
        $data['header'] = $header;


        $this->load->view('articles/index', $data);


    }

    public function content_get($id, $article_name)
    {

        $article_name = urldecode($article_name);

        $input = array('id' => $id);
        $input_file = array('id_article' => $id);

        $result = $this->articles->GetArticle($input);
        $result_files = $this->articles->GetFiles($input_file);
        $result_Comment = $this->articles->GetComment($input_file);

        $titlePage = str_replace('%20', ' ', $article_name);

        if (count($result) <= 0)
            redirect(base_url());
        $data['data'] = $result;

        $files = [];
        foreach ($result_files as $item):
            $fileType = pathinfo($item['name'], PATHINFO_EXTENSION);
            array_push($files, array('name' => rtrim($item['name'], '.' . $fileType), 'type' => strtolower($fileType)));

        endforeach;

        $data['files'] = $files;
        $data['comment'] = $result_Comment;


        if ($this->isWriter($result[0]['id_user']))
            $data['isWriter'] = true;
        else
            $data['isWriter'] = false;

        $header = $this->SetHeader($titlePage);
        $this->load->view('header/header', $header);
        $this->load->view('articles/article', $data);

    }

    public function delete_get($id)
    {


        $id_user = $this->GetArticleUserID($id);

        if (!$this->isWriter($id_user))
            redirect(base_url() . '/article');

        $this->articles->DeleteArticle($id);
        redirect(base_url());


    }

    public function edit_get($id)
    {

        $id_user = $this->GetArticleUserID($id);

        if (!$this->isWriter($id_user))
            redirect(base_url() . '/article');


        $this->session->set_userdata(
            'author', array(
            'id' => $id
        ));

        $input = array('id' => $id);
        $input_file = array('id_article' => $id);


        $result = $this->articles->GetArticle($input);
        $result_files = $this->articles->GetFiles($input_file);

        $data['data'] = $result;
        $data['result_files'] = $result_files;
        $header = $this->SetHeader("Edit " . $this->session->userdata('subject')['subject']);
        $this->load->view('header/header', $header);
        $this->load->view('articles/edit', $data);

    }

    public function create_get()
    {
        $header = $this->SetHeader("New Article");
        $this->load->view('header/header', $header);
        $this->load->view('articles/create');

    }

    public function GetArticleUserID($id)
    {
        $result = $this->articles->GetUserId(array('id' => $id));
        return $result[0]['id_user'];
    }

    public function isWriter($id_user)
    {
        return $id_user == $this->Get_userId();
    }

    public function insert_post()
    {
        $this->load->library('form_validation');
        $this->form_validation->set_rules('title', 'title', 'required');
        $this->form_validation->set_rules('author', 'author', 'required');
        $this->form_validation->set_rules('content', 'content', 'required');
        if ($this->form_validation->run() == FALSE) {
            show_error(validation_errors());
        }

        $input = array(
            'title' => $this->input->post('title'),
            'author' => $this->input->post('author'),
            'body' => $this->input->post('content'),
            'id_user' => $this->Get_userId(),
            'id_subject' => $this->session->userdata('subject')['id_subject'],
            'year' => $this->session->userdata('subject')['year'],
        );

        if ($id_article = $this->articles->Inserts($input)) {
            $url = base_url() . 'article/show/' . urlencode($this->session->userdata('subject')['id_subject']) . '/' . urlencode($this->session->userdata('subject')['subject']) . '/' . urlencode($this->session->userdata('subject')['year']);
            $this->insertFile($id_article, $url);
        } else
            show_error("Error Insert");


    }

    public function insertFile($id_article, $url)
    {
        $count_file = count($_FILES['fileToUpload']['name']);

        if ($_FILES['fileToUpload']['size'][0] <= 0)
            redirect($url);

        for ($i = 0; $i < $count_file; $i++) {

            $file_name = array('id_article' => $id_article, 'name' => $_FILES['fileToUpload']['name'][$i]);

            $FilePath = "./public/uploadFiles/" . $_FILES['fileToUpload']['name'][$i];
            $tmpFilePath = $_FILES['fileToUpload']['tmp_name'][$i];
            if (!move_uploaded_file($tmpFilePath, $FilePath)) {
                show_error('Error Upload File');
            }
            $this->articles->InsertsFile($file_name);

        }//for
        redirect($url);
    }

    public function update_post()
    {
        $id_user = $this->GetArticleUserID($this->session->userdata('author')['id']);

        if (!$this->isWriter($id_user))
            redirect(base_url() . '/article');

        $input = array(
            'title' => $this->input->post('title'),
            'author' => $this->input->post('author'),
            'body' => $this->input->post('content'),
            'id_user' => $this->Get_userId(),
            'id_subject' => $this->session->userdata('subject')['id_subject'],
            'year' => $this->session->userdata('subject')['year'],
        );

        if ($this->articles->Updates($input, $id_article = $this->session->userdata('author')['id'])) {
            $this->session->unset_userdata('author');
            $url = base_url() . 'article/show/' . urlencode($this->session->userdata('subject')['id_subject'])
                . '/' . urlencode($this->session->userdata('subject')['subject'])
                . '/' . urlencode($this->session->userdata('subject')['year']);
            $this->insertFile($id_article, $url);
        } else
            show_error("Error Insert");


    }

    public function GetComment_get($id)
    {

        $input_file = array('id_article' => $id);
        $result_Comment = $this->articles->GetComment($input_file);
        $html = '';

        foreach ($result_Comment as $row) {
            $data['comment'] = $row['comment'];
            $data['StudentName'] = $row['StudentName'];
            $data['UserID'] = $row['id_user'];
            $data['id'] = $row['id'];
            $data['UserIDLogin'] = $this->Get_userId();
            $html .= $this->load->view('articles/comment', $data, true); // returns view as data
        };
        echo $html;

    }

    public function GetCommentUserId($id)
    {

        $input_file = array('id' => $id);
        $result_Comment = $this->articles->GetCommentUserId($input_file);

        return $result_Comment[0]['id_user'];

    }

    public function insertComment_post()
    {

        $data['id_user'] = $this->Get_userId();
        $data['StudentName'] = $this->Get_userName();
        $data['id_article'] = $this->input->post('id_article');
        $data['comment'] = $this->input->post('comment');

        $this->articles->InsertComment($data);

    }

    public function updateComment_post()
    {

        $id = $this->input->post('id');

        $ID_User = $this->GetCommentUserId($id);
        if ($this->Get_userId() != $ID_User)
            return;
        $comment = $this->input->post('comment');
        $data['comment'] = $comment;
        $data['id'] = $id;

        $this->articles->UpdateComment($data);

    }

    public function deleteComment_post()
    {

        $id = $this->input->post('id');

        $ID_User = $this->GetCommentUserId($id);
        if ($this->Get_userId() != $ID_User)
            return;

        $data['id'] = $id;

        $this->articles->DeleteComment($data);

    }

    public function removeFiles_post()
    {

        $input = $this->input->post('id');
        $this->articles->RemoveFile($input);

    }

}