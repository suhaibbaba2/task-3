<?php

class Student extends MY_Controller
{
    public function index_get()
    {
        if ($this->isLogin()) {
            $this->Register_get();
        }else{
            redirect(base_url());
        }

    }

    public function Register_get()
    {
        $header = $this->SetHeader('Add Student ');

        if ($this->isLogin()) {
            $this->load->view('header/header', $header);
            $this->load->view('student/register');
        } else {
            redirect(base_url());
        }
    }

    public function insert_post(){
        if (!$this->isLogin()) {
            redirect(base_url());
        }
            //Not Submit
        if (!isset($_POST['submit']) || empty($_POST)) {
            redirect(base_url());
        }

        $this->load->model('authentication/users');

        $firstName = $this->input->post('firstname');
        $email = $this->input->post('email');
        $data = array(
            'firstname' => $firstName,
            'lastname' => $this->input->post('lastname'),
            'email' => $email,
            'password' => sha1($this->input->post('password')),
            'type' => 'student'
        );


        if ($id=$this->users->insets($data)) {
            $login = array('name' => $firstName, 'email' => $email, 'id'=>$id,'type' => 'student');
            $this->Set_login($login);
            redirect(base_url());
        } else {
            show_error("ERROR INSERT Data to Database");
        }


    }
}