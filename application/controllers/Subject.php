<?php

class Subject extends MY_Controller
{

    public function __construct()
    {
        parent::__construct();

        if (!$this->isLogin())
            redirect(base_url() . 'login');

        $this->load->model('subjects/subjects');
        echo "hi";

    }

        public function index_get($year = '')
    {


        if (empty($year))
            $year = date("Y");

        $this->subjects_get($year);
    }

    public function index_post()
    {

        redirect(base_url());
    }

    public function subjects_get($year = null)
    {
        if (empty($year))
            $year = date("Y");

        $header = $this->SetHeader('subjects');
        $data['data'] = $this->GetSubjects_get($year);
        $data['year'] = $year;
        $data['isLogin'] = $this->isLogin();

        $data['isTeacher'] = strtolower($this->Get_Type()) == "teacher";

        $this->load->view('header/header', $header);
        $this->load->view('subjects/index', $data);
    }

    public function GetSubjects_get($year)
    {

        $result = $this->subjects->GetAll($year);

        return $result;
    }

    public function create_get()
    {
        if ($this->isLogin() && strtolower($this->Get_Type()) == "teacher") {
            $header = $this->SetHeader('create');

            $this->load->view('header/header', $header);
            $this->load->view('subjects/create');
        } else {
            redirect(base_url());
        }
    }

    public function insert_post()
    {

        $this->load->library('form_validation');
        $this->form_validation->set_rules('name', 'name', 'required');
        $this->form_validation->set_rules('year', 'year', 'required');
        if ($this->form_validation->run() == FALSE) {
            show_error(validation_errors());
        }

        $data = array(
            'subject' => $this->input->post('name'),
            'year' => $this->input->post('year'),
            'id_user' => $this->Get_userId()
        );

        $result = $this->subjects->inserts($data);
        if (!$result)
            show_error();
        else
            redirect(base_url() . 'subject');
    }

}