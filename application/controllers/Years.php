<?php

class Years extends MY_Controller{

    public function __construct()
    {
        parent::__construct();
    }

    public function index_get ()
    {
        if (!$this->isLogin())
            redirect(base_url().'login');

        $this->years_get();
    }

    public function index_post (){
        if (!$this->isLogin())
            redirect(base_url().'login');

        $this->years_get();
    }

    public function years_get(){
        if (!$this->isLogin())
            redirect(base_url().'login');

        $header =$this->SetHeader('years');

        $data['data']=$this->Getyears_get();

        $this->load->view('header/header',$header);
        $this->load->view('years',$data);

    }

    public function Getyears_get(){
        if (!$this->isLogin())
            redirect(base_url().'login');

        $this->load->model('years/year');
        $result=$this->year->GetAll();

        return $result;
    }

}