<?php

class Authentication extends MY_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->model('authentication/users');

    }
    public function _remap_get()
    {
    }


    public function index_post()
    {
        if(!$this->isLogin())
            $this->loginPage_get();
        redirect(base_url());
    }

    public function index_get()
    {
        if(!$this->isLogin())
            $this->loginPage_get();

        redirect(base_url());
    }


    public function loginPage_get()
    {
//        $data['page_title'] = 'Login';
        $this->session->set_userdata('islogin', 'false');
        $header = $this->SetHeader('Login');

        if (!($this->isLogin())) {
            $this->load->view('header/header', $header);
            $this->load->view('authentication/login');
        } else {
            $this->session->set_userdata('islogin', 'true');
            redirect(base_url());
        }

    }

    public function registerPage_get()
    {
        $header = $this->SetHeader('Register');

        if (!$this->isLogin()) {
            $this->load->view('header/header', $header);
            $this->load->view('authentication/register');
        } else {
            redirect(base_url());
        }

    }

    public function logout_get()
    {
        $this->session->unset_userdata('user');
        $this->session->set_userdata('islogin', 'false');

        redirect(base_url());
    }

    public function checklogin_post()
    {

        //Not Submit
        if (!isset($_POST['submit']) || empty($_POST)) {
            redirect(base_url());
        }

        $password = $this->input->post('password');
        $email = $this->input->post('email');
        $data = array('email' => $email, 'password' => sha1($password));

        $result = $this->users->checkLogin($data);

        if ($result == false)
            redirect(base_url());

        $login = array('name' => $result['name'],'id'=> $result['id'],'email' => $email, 'type' => $result['type']);
        $this->Set_login($login);

        redirect(base_url());


    }

    public function RegisterInsert_post()
    {
        //Not Submit
        if (!isset($_POST['submit']) || empty($_POST)) {
            redirect(base_url());
        }

        $firstName = $this->input->post('firstname');
        $email = $this->input->post('email');
        $data = array(
            'firstname' => $firstName,
            'lastname' => $this->input->post('lastname'),
            'email' => $email,
            'password' => sha1($this->input->post('password')),
            'type' => 'teacher'
        );


        if ($id=$this->users->insets($data)) {
            $login = array('name' => $firstName, 'email' => $email, 'id'=>$id,'type' => 'teacher');
            $this->Set_login($login);
            redirect(base_url());
        } else {
            show_error("ERROR INSERT Data to Database");
        }

    }

    // Error Input go To BaseURL
    public function registerInsert_get()
    {
        $header['page_title'] = 'Register';
        $this->load->view('header/header', $header);
        $this->load->view('authentication/register');
    }


}