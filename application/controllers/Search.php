<?php

class Search extends MY_Controller
{
    public function index_post(){
        $input_Subjects=array('subject'=>$this->input->post('input'));
        $input_Articles=array('title'=>$this->input->post('input'));
        $this->load->model('search/searchs');
        $result_Subjects=$this->searchs->GetSearch_Subjects($input_Subjects);
        $result_Articles=$this->searchs->GetSearch_Articles($input_Articles);

        $result=array(
            'subjects'=>$result_Subjects,
            'articles'=>$result_Articles
            );
        echo json_encode($result);
    }
}