function SetWidth() {
    var width = $("#search").css('width');
    $("#livesearch").css('width', width)
}

function Search(str, baseUrl) {
    var input = {'input': str}
    $("#livesearch").empty();

    $.ajax({
        url: baseUrl + 'search/index',
        data: input,
        type: 'POST',
        cache: false,
        success: function (data) {
            var base_url = baseUrl;

            var array_result = JSON.parse(data);

            var result_subjects = array_result['subjects'];
            var result_articles = array_result['articles'];

            if (result_subjects.length <= 0 && result_articles.length <= 0) {
                var div = "<h5 class='text-center'>No Subjects Found</h5>";
                $('#livesearch').append(div)
            }

            for (var i = 0; i < result_subjects.length; i++) {
                var url = base_url + 'article/show/' + result_subjects[i]['id'] + '/' + encodeURIComponent(result_subjects[i]['subject']) + '/' + result_subjects[i]['year'];
                var div = "<a href=" + url + " style=text-transform:capitalize>" +
                    "<img src='" + base_url + "public/images/folder-blue.png' style='width: 24px;height: 24px;margin-right: 20px;'> " + result_subjects[i]['subject'] + "</a>";

                $('#livesearch').append(div)
            }

            for (var i = 0; i < result_articles.length; i++) {
                var url = base_url + 'article/content/' + result_articles[i]['id'] + '/' + encodeURIComponent(result_articles[i]['title']);
                var div = "<a href=" + url + " style=text-transform:capitalize>" +
                    "<img src='" + base_url + "public/images/article-red.png' style='width: 24px;height: 24px;margin-right: 20px;'> " + result_articles[i]['title'] + "</a>";
                $('#livesearch').append(div)
            }
            SetWidth()
            $(".dropdown-content").css('display', 'block')
        }
    });
}//function

$(document).click(function (e) {

    if (e.target.id == 'search') {
        $("#livesearch").css('display', 'block')
    }
    else if (e.target.id != 'livesearch') {
        $("#livesearch").css('display', 'none');
    }
});

function ShowSearchBox() {
    $('.search-box').fadeToggle();
}




